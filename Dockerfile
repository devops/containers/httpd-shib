FROM centos:7

COPY yum/shibboleth.repo /etc/yum.repos.d/
RUN yum -y update \
	&& \
	yum -y install \
	httpd \
	httpd-devel \
	httpd-tools \
	less \
	mod_ssl \
	shibboleth \
	wget \
	&& \
	yum -y clean all

# Shibboleth
WORKDIR /etc/shibboleth
COPY shibboleth/* ./
RUN wget -q https://shib.oit.duke.edu/duke-metadata-2-signed.xml \
	&& chmod 644 duke-metadata-2-signed.xml \
	&& wget -q https://shib.oit.duke.edu/idp_signing.crt \
	&& chmod 644 idp_signing.crt \
	&& chmod 755 shibd-redhat \
	&& echo $'export LD_LIBRARY_PATH=/opt/shibboleth/lib64:$LD_LIBRARY_PATH' > /etc/sysconfig/shibd \
	&& chmod 644 /etc/sysconfig/shibd

COPY httpd/* /etc/httpd/conf.d/
COPY www/* /var/www/

COPY container-entrypoint.sh /usr/bin/
RUN chmod 755 /usr/bin/container-entrypoint.sh

RUN  mkdir -p -m 644 /httpd-conf.d
VOLUME /httpd-conf.d

EXPOSE 80 443
ENTRYPOINT ["container-entrypoint.sh"]
CMD ["/usr/sbin/httpd", "-DFOREGROUND"]
