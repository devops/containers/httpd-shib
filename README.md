# httpd-shibboleth container base image

This image provides Apache HTTPD 2.4 server on ports 80 and 443 running in the foreground
and a Shibboleth SP daemon running in the background.

The httpd server requires the environment variable `HTTPD_SERVER_NAME` at image build or
container run time to be set to the canonical host name of the service.

Note that TLS certs and keys for HTTPS must be copied in to the locations referenced
in any provided config.  By default, the SSL config expects Docker secrets named `ssl_cert`,
`ssl_cert_chain`, and `ssl_cert_key`.

Additional Apache config can be mounted using the volume at `/httpd-conf.d`.

The shibboleth config at `/etc/shibboleth/shibboleth2.xml` can be used as is, with two
requirements:

- The CSR cert and key used for the Shib registration are expected be Docker secrets named
  `shib_cert` and `shib_key`.

- The Shibboleth entity ID must be inserted in `/etc/shibboleth/shibboleth2.xml` in place
  of the string `__SHIB_ENTITY_ID__`, for example:

  ```
  RUN sed -i -e 's|__SHIB_ENTITY_ID__|https://library.duke.edu|' /etc/shibboleth/shibboleth2.xml
  ```

  In the base image, the environment variable `SHIB_ENTITY_ID` will be used as a replacement
  value, if defined.

## Build and Run

Example:

	$ docker build -t httpd-shib .

    $ docker run --rm -e HTTPD_SERVER_NAME=library.duke.edu -e SHIB_ENTITY_ID=https://library.duke.edu
