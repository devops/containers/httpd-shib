#!/bin/bash

rm -f /etc/httpd/run/httpd.pid \
      /var/run/shibboleth/shibd.pid \
      /var/lock/subsys/shibd

if [ -n "${SHIB_ENTITY_ID}" ]; then
    sed -i -e 's|__SHIB_ENTITY_ID__|'"${SHIB_ENTITY_ID}"'|' /etc/shibboleth/shibboleth2.xml
fi

/etc/shibboleth/shibd-redhat start

exec "$@"
